===========================
 Working Memory Experiment
===========================

A browser-based task for measuring working memory capacity in children.

Overview
========

This is a simple Memory Game to test the capacity of a child's working
memory. The children are shown a 3x3 grid with fruit on some of the
grid-boxes. The experimenter says "I want you to press this and this and this"
as they touch between one and five of the fruit. The child then tries to touch
the same fruit in the same order.

Installation
============

Requires Python 3.4 or later, and should run on OSX, Windows and Linux, though
I've only tested on Linux.

Assuming Python and pip are installed, you can install the necessary packages
with ::

  $ pip install -r requirements.txt


Experiment Setup
================

We currently run the experiment on a touchscreen lenovo x220 with a swivel
tablet screen running Debian 8 (jessie). We run the experiment in tablet
configuration with a usb number pad. The experiment is programmed to use the "5"
key on the numberpad to switch who the current user (experimenter or
participant) and the "6" key to move to the next trial.

The Interface
=============

The trial grids are laid out like this ::

    _________________________________________________________   __
   |                   .                   .                 |    |
   |        1          .         2         .       3         |    |
   |                   .                   .                 |    |
   |.........................................................|    |
   |                   .                   .                 |    |
   |        4          .         5         .       6         |    |
   |                   .                   .                 |    |--- The Grid
   |.........................................................|    |    (for fruit)
   |                   .                   .                 |    |
   |        7          .         8         .       9         |    |
   |                   .                   .                 |    |
   |_________________________________________________________|  __|   __
   |                                                         |          |
   | Trial #  | Whose Turn | Correct Sequence  |  Model Grid |          |--- The Secret
   |_________________________________________________________|        __|    Display Panel


Trial #
  The number of the trial you're currently on. This will restart when
  you finish the practice round and start the real one.

Whose Turn
  Will read either ``E`` or ``P`` for experimenter or participant. Starts
  with ``P``. Is toggled by pressing the ``5`` key on the numpad.

Correct Sequence
  A series of numbers (1-9) that represent the sequence of fruits the
  experimenter is to touch and the participant is to imitate. The numbers
  represent a spot on the grid. Use the ``Model Grid`` as a helper to figure
  out where on the board a number is.

Model Grid
  A small grid of numbers showing the experimenter the numbering of
  the grid items. It makes reading the ``Correct Sequence`` easier.

Running
=======

In order to run the experiment, execute ::

  $ python app.py

And navigate a web browser to http://localhost:5000/. We use Firefox, but it's
likely to work in chrome as well.

Fill out the meta data on the first page (experimenter and subject ids) and
follow the protocol:

1. Look at the ``Correct Sequence`` hint and ``Model Grid`` helper (see `The
   Interface`_) in order to determine the sequence to present to the participant
   for this trial.

2. Use the touchscreen to press the sequence of fruits. Say "this" out loud each
   time you touch one. For example, for three fruits you'd say "I want *this*
   and *this* and *this*".

3. After you've done this you need to tell the program that it's not longer the
   experimenter that's touching the screen, but the participant. You do this by
   pressing the ``5`` key on the numpad. You should see the ``Whose Turn`` (see
   `The Interface`_) indicator change from ``E`` to ``P``. You can keep pressing
   it to toggle back and forth between the two.

4. You should now ask the child to repeat what you just did- touching the same
   fruits in the same order.

5. (optional) If the child is confused and you need to show them the sequence
   again, simply press the ``5`` key again so that the ``Whose Turn`` indicator
   shows ``P`` and go back to step 2.

6. Press ``6`` on the numpad to advance to the next trial. Go back to step 1 and
   repeat until the experiment ends.

Retrieving Data
===============

At the completion of the experiment a csv with the trial data is written to the
data/ directory in the code folder. If the experiment is not run to completion,
you can force the data-dump procedure for all trials by visiting the url
http://localhost:5000/dump-data . The data files have a date/time prefix that
corresponds to when the data was dumped, followed by the participant id.

The csv files themselves have the following columns::

  trialnum, participant, correct-sequence, observed-sequence, correct?, fruits, delay-touch

trialnum
  A letter-number pair indicating experiment phase (E/P) and trial number. P3 =
  3rd trial of the practice phase, E5 = 5th trial of the experimental phase.

participant
  identifies who was touching the screen to generate the data for a given row
  (experimenter/participant)

correct-sequence
  the grid-indexes of the grid cell touch sequence the experimenter should have
  shown to the participant, and that the participant is attempting to remember
  and perform.

observed-sequence
  the grid-indexes that the were actually touched by `participant`

correct?
  a scoring of the experimenter or participants accuracy. 1 for correct, 0 for
  incorrect.

fruits
  a space-separated list of the names of the fruit images displayed during the
  trial.

delay-touch
  TODO: describe delay touch
