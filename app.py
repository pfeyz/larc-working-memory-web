from flask import Flask, url_for, request, Response
import csv
import json
import sqlite3

from datetime import datetime
from io import StringIO
from itertools import groupby
from os.path import splitext, dirname, realpath, isfile, isdir, join as pjoin
from pprint import pprint

CURDIR = dirname(realpath(__file__))
DB_FILENAME = pjoin(CURDIR, 'data.db')

def initialize_db(db_filename):
    print("Initializing database '{}'".format(db_filename))
    conn = sqlite3.connect(db_filename)
    cur = conn.cursor()
    with open('schema.sql') as fh:
        schema = fh.read()
    cur.executescript(schema)
    conn.commit()
    conn.close()

app = Flask(__name__)

@app.route('/')
def index():
    return open('index.html').read()

@app.route('/event', methods=['POST'])
def post_event():
    if request.method == 'POST':
        conn = sqlite3.connect(DB_FILENAME)
        timestamp = request.form['timestamp']
        uid = request.form['uid']
        data = request.form['data']
        cur = conn.cursor()
        cur.execute('INSERT INTO events(timestamp, uid, data) VALUES(?, ?, ?)',
                    (timestamp, uid, data))
        conn.commit()
        return ''

@app.route('/props', methods=['POST'])
def post_props():
    if request.method == 'POST':
        conn = sqlite3.connect(DB_FILENAME)
        timestamp = request.form['timestamp']
        uid = request.form['uid']
        data = request.form['data']
        cur = conn.cursor()
        cur.execute('INSERT INTO props(timestamp, uid, data) VALUES(?, ?, ?)',
                    (timestamp, uid, data))
        conn.commit()
        return ''

def get_datalist():
    " Returns a list of all subjects and their uids "
    conn = sqlite3.connect(DB_FILENAME)
    cur = conn.cursor()
    cur.execute('SELECT DISTINCT uid FROM events')
    return [i[0] for i in cur.fetchall()]

def normalize_trialdata(uid):
    """Returns the trial data for a session identified by `uid`

    Arguments:
    `uid`: the client-side generated random uid for a given session

    Returns:

    A tuple of (metadata, trialdata), where

      - metadata is a tuple of (participant, id)
      - trialdata is a dict with the keys:
        ['timestamp', 'event', 'trial_num', 'user', 'cell_index', 'image']


    """
    conn = sqlite3.connect(DB_FILENAME)
    cur = conn.cursor()
    cur.execute("SELECT data FROM props WHERE uid=?", (uid,))
    prop_data = json.loads(cur.fetchone()[0])
    trials = prop_data['props']['trials']
    cur = conn.cursor()
    cur.execute("SELECT timestamp, uid, data FROM events WHERE uid=?", (uid,))
    events = []
    participant = experimenter = None
    for timestamp, uid, json_str in cur.fetchall():
        data = json.loads(json_str)
        app_state = data['state']
        event, from_state, to_state = data['event'][:3]
        if event == 'submitData':
            event_info = data['event'][3]
            participant = event_info['partId']
            experimenter = event_info['expId']
        if event == 'nextTrial':
            absolute_index = app_state['trialNumber']
            # events.append({'timestamp': timestamp,
            #                'event': event,
            #                'trial_num': trials[absolute_index]['index']})
        if event == 'cellClick':
            event_info = data['event'][3]
            images = [splitext(cell['imageSrc'])[0]
                      for cell in app_state['cells']]
            cell_index = event_info['index']
            absolute_index = app_state['trialNumber']
            events.append({'timestamp': timestamp,
                           'target_sequence': trials[absolute_index]['sequence'],
                           'event': event,
                           'trial_num': trials[absolute_index]['index'],
                           'user': app_state['activeUser'],
                           'cell_index': cell_index,
                           'image': images[cell_index].split('/')[-1]
                       })
    return ([participant, experimenter], events)

def pluck(key, list_of_dicts):
    " Maps dict key selection over a list "
    return [d[key] for d in list_of_dicts]

def timestamp_diffs(timestamps):
    " Calculates the durations between timestamps (ms) x"
    return [timestamps[i + 1] - timestamps[i]
            for i in range(len(timestamps) - 1)]

def merge_events(events):
    """Merges a list of event dicts together as a single row of data

    """
    timestamps = timestamp_diffs(pluck('timestamp', events))
    return [pluck('cell_index', events),
            pluck('image', events),
            timestamps]

def format_data(trialdata):
    """Turns a trialdata object into a nested list suitable for writing out as
    tabular data

    """
    _, eventdata = trialdata
    def key(event):
        return event['trial_num'], event['user']
    data = []
    for (trial_num, user), events in groupby(eventdata, key):
        events = list(events)
        indexes, images, timestamps = merge_events(events)
        target = events[0]['target_sequence']
        correct = 1 if indexes == target else 0
        data.append([trial_num,
                     'participant' if user == 'P' else 'experimenter',
                     target, indexes, correct, images, timestamps])
    return data

def is_str_int(x):
    try:
        int(x)
    except:
        return False
    return True

def list_of_num(x):
    return all([is_str_int(item) for item in x])

def process_list_for_csv(item):
    if list_of_num(item):
        item = [int(x) + 1 for x in item]

    return ' '.join(map(str, item))

def trialdata_csv(trialdata):
    out = StringIO()
    writer = csv.writer(out)
    writer.writerow(['TRIALNUM',
                     'PARTICIPANT',
                     'CORRECT-SEQUENCE',
                     'OBSERVED-SEQUENCE',
                     'CORRECT?',
                     'FRUITS',
                     'DELAY-TOUCH'])
    for row in trialdata:
        writer.writerow([process_list_for_csv(item)
                         if type(item) is list else str(item)
                         for item in row])
    return out

@app.route('/data/')
def list_datasets():
    return '\n'.join(['/data/' + i + '.csv' for i in get_datalist()])

@app.route('/data/<uid>.csv')
def get_data(uid):
    if uid not in get_datalist():
        return 'unknown session id'
    return Response(trialdata_csv(format_data(normalize_trialdata(uid))).getvalue(),
                    mimetype='text/csv')

def sjoin(sep, items):
    return sep.join(map(str, items))

@app.route('/dump-data', methods=['POST'])
def dump_data_route():
    if request.method == 'POST':
        uid = request.form['uid']
        dump_data(uid)
    return ''

def dump_data(uid):
    now = datetime.now()
    metadata, data = normalize_trialdata(uid)
    participant, _ = metadata
    data = trialdata_csv(format_data([metadata, data]))
    filename = sjoin('-', [now.year, now.month, now.day])
    filename += '_' + sjoin('-', [now.hour, now.minute])
    filename += '_' + participant + '.csv'
    filename = pjoin('data', filename)
    with open(filename, 'wb') as fh:
        fh.write(bytes(data.getvalue(), 'utf8'))

if __name__ == "__main__":
    # initialize the DB if it doesn't exist
    if not isfile(DB_FILENAME):
        if isdir(DB_FILENAME):
            raise Exception('A directory name conflicts with the database name "{}"'
                            .format(DB_FILENAME))
        initialize_db(DB_FILENAME)
    app.run()
