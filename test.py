import time
import random
import string
from multiprocessing import Pool
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

BASE_URL = "http://localhost:5000"
def dec(n):
    return int(n) - 1
def run_trial(_):
    driver = webdriver.Firefox()
    driver.get(BASE_URL)
    name1, name2, start_button = driver.find_elements_by_tag_name("input")
    name1.send_keys('Test Experimenter')
    name2.send_keys('Test Participant')
    time.sleep(0.5)
    start_button.click()
    wait = WebDriverWait(driver, 26)
    for i in range(20):
        print('trial', i)
        chain = action_chains.ActionChains(driver)

        targets = list(map(dec, driver.find_element_by_id('target-sequence').text.split(' ')))
        cells = driver.find_elements_by_class_name('cell')
        print(targets)
        for index in targets:
            chain.click(cells[index])
        chain.perform()
        driver.find_element_by_class_name('grid-container').send_keys(Keys.NUMPAD5)
        time.sleep(2)
        chain = action_chains.ActionChains(driver)
        cells = driver.find_elements_by_class_name('cell')
        for index in targets:
            chain.click(cells[index])
        chain.perform()
        time.sleep(1)
        driver.find_element_by_class_name('grid-container').send_keys(Keys.NUMPAD6)
        wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'cell')))
        time.sleep(2)

if __name__ == "__main__":
    # num_parallel = int(argv[1])
    # pool = Pool(processes=num_parallel)
    # pool.map(run_trial, range(num_parallel))
    run_trial(1)

#     slider = browser.find_element_by_class_name("ui-slider-handle")
#     inflate = browser.find_element_by_id("inflate")
#     wait = WebDriverWait(browser, 10)
#     for i in range(num):
#         chain = action_chains.ActionChains(browser)
#         amt = random.randint(-300, 300)  # amount to slide slider
#         wait.until(
#             lambda _: 'disabled' not in inflate.get_attribute('class').split(' '))
#         chain.drag_and_drop_by_offset(slider, amt, 0)
#         chain.perform()
#         inflate.click()

#     next_btn = wait.until(
#         EC.element_to_be_clickable((By.ID, "next"))
#     )

#     next_btn.click()
#     time.sleep(2)
#     browser.quit()
