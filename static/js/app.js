/** @jsx React.DOM */
/*global require */

var React = require('react'),
    _     = require('underscore'),
    StateMachine = require('javascript-state-machine'),
    decycle = require('cycle').decycle,
    utils = require('./utils'),
    uid = require('uid')(10);

// A dataform component that collects the IDs of the participant and
// experimenter in a simple form. Upon form submission, calls props.handleSubmit
// with its own state as the only argument.
var DataForm = React.createClass({
    getInitialState: function(){
        return {
            expId: "",
            partId: ""
        };
    },
    render: function(){
        return <div className="data-form-container"> <h1>WM</h1> <div className="data-form">
              <form onSubmit={this.handleSubmit} >
                <label> <span>Experimenter ID</span>
                  <input type="text" value={this.state.expId}
                                     onChange={this.handleChange("expId")} />
                </label>
                <label> <span>Participant ID</span>
                  <input type="text" value={this.state.partId}
                                     onChange={this.handleChange("partId")} />
                </label>
                <input type="submit" value="Start Experiment"/>
            </form>
            </div> </div>;
    },
    handleSubmit: function(event){
        if (this.state.expId.length && this.state.partId.length){
            this.props.handleSubmit(this.state);
        }
        event.preventDefault();
    },
    handleChange: function(name){
        var that = this;
        return function(event) {
            var state = {};
            state[name] = event.target.value;
            that.setState(state,function(){
                // console.log(that.state);
            });
        };
    }
});

// A single grid cell on the fruit grid. When pressed, calls props.handleClick
// with a the cell object (created by Experiment.makeCells) and the actual dom
// node passed as arguments. ImageGrid handles initializing GridCell components.
var GridCell = React.createClass({
    handleClick: function(){
        this.props.handleClick(this.props.cell, this.refs.element.getDOMNode());
    },

    render: function(){
        return <div className={this.props.cell.animating ? "no-select cell fader" : "no-select cell"}
                    onMouseDown={this.handleClick}
                    ref="element">
	           <img src={this.props.cell.imageSrc} alt="" />
    	       </div>;
    }
});

// The grid holding the image cells. Delegates all event handling to
// its GridCell components.
var ImageGrid = React.createClass({
    render: function(){
        return <div className="grid-container">
                   {this.makeRow(this.props.cells.slice(0, 3))}
                   {this.makeRow(this.props.cells.slice(3, 6))}
                   {this.makeRow(this.props.cells.slice(6, 9))}
               </div>;
    },

    makeRow: function(cells){
        if (cells.length != 3){
            return <div></div>;
        };
        return <div className="row">
                   <GridCell handleClick={this.props.handleCellClick} cell={cells[0]} />
                   <GridCell handleClick={this.props.handleCellClick} cell={cells[1]} />
                   <GridCell handleClick={this.props.handleCellClick} cell={cells[2]} />
               </div>;
    }
});

// a "cheat sheet" for the experimenter below the fruit grid.
var HinterPanel = React.createClass({
    render: function(){
        var sequence = this.props.trial.sequence.map(function(n){ return n + 1; }).join(' ');
        return <div className="hinter no-select">
                   <div> {this.props.trial.index} </div>
                   <div> {this.props.activeUser} </div>
                   <div id="target-sequence"> {sequence} </div>
                   <div className="mini-grid">
                     <div className="row">
                       <div className="cell">1</div>
                       <div className="cell">2</div>
                       <div className="cell">3</div>
                     </div>
                     <div className="row">
                       <div className="cell">4</div>
                       <div className="cell">5</div>
                       <div className="cell">6</div>
                     </div>
                     <div className="row">
                       <div className="cell">7</div>
                       <div className="cell">8</div>
                       <div className="cell">9</div>
                   </div>
                 </div>
               </div>;
    }
});

// Contains the ImageGrid and HinterPanel.
var GridView = React.createClass({
    render: function(){
        return <div className="grid-container">
                   <ImageGrid cells={this.props.cells}
                              handleCellClick={this.props.handleCellClick} />
                   <HinterPanel trial={this.props.trial}
                                activeUser={this.props.activeUser} />
               </div>;
    }
});

var FeedbackView = React.createClass({
    render: function(){
        return <div className="feedback-container">
                   <img src="static/img/star.jpg" />
               </div>;
    }
});

var Experiment = React.createClass({
    componentDidMount: function(){
        window.addEventListener('keydown', this.handleKeyPress);
        window.comp = this;
        this.props.fsm.start(this);
    },
    log: function(event){
        var log = this.state.eventLog.slice();
        log.push([Date.now(), event]);
        this.setState({eventLog: log});
    },
    finished: function(){
        return this.state.trialNumber == this.props.trials.length - 1;
    },
    getInitialState: function(){
        var trial = this.props.trials[0];
        return {experimenter: null,
                participant: null,
                view: "dataCollect",
                cells: this.makeCells(trial),
                trialNumber: 0,
                activeUser: 'E'
               };
    },

    currentTrial: function(){
        return this.props.trials[this.state.trialNumber];
    },

    handleFormSubmit: function(data){
        this.props.fsm.submitData(data);
    },

    // Advances trial or switches current user.
    handleKeyPress: function(event){
        // console.log(event);
        // this.log(event);
        var keyCode = event.keyCode,
            trial = this.currentTrial(),
            that = this,
            state = null;
        if(keyCode === 101 || keyCode == 12){
            this.props.fsm.switchUser();
        }
        if(keyCode === 102 || keyCode == 39){
            this.props.fsm.finishTrial();
        }
    },

    handleCellClick: function(cell, element){
        // console.log(this.state.trialNumber);
        this.props.fsm.cellClick(cell, element);
    },

    makeCells: function(trial){
        var mask = trial.mask,
            sequence = trial.sequence,
            images = trial.images;
        // console.log('makeCells');
        if (mask.length !== images.length){
            throw new Error('image array must be same length as mask array');
        }
        sequence.forEach(function(cellNum){
            if (mask.indexOf(cellNum) == -1){
                throw new Error('sequence array must be subset of mask array');
            }
        });
        var cells = [];
        for(var i=0; i<9; i++){
            cells.push({imageSrc: "static/img/blank.jpg",
                        index: i,
                        touchable: false,
                        animating: false});
        }
        mask.forEach(function(cellNum, n){
            cells[cellNum].imageSrc = images[n];
        });

        sequence.forEach(function(cellNum){
            cells[cellNum].touchable = true;
        });
        return cells;
    },

    refreshedCells: function(cells){
        var that = this;
        return this.state.cells.map(function(c){
            return _.extend(c, {
                touchable: that.currentTrial().sequence.indexOf(c.index) > -1});
        });
    },

    pathToComponent: function(path){
        // console.log(path);
        var paths = {
            'dataCollect': <DataForm handleSubmit={this.handleFormSubmit} />,
            'grid': <GridView cells={this.state.cells}
                              trial={this.currentTrial()}
                              activeUser={this.state.activeUser}
                              handleCellClick={this.handleCellClick}
                              onKeyDown={this.handleKeyPress} />,
            'feedback': <FeedbackView />,
            'end': <div className="feedback-container">
                       <h1>Thanks for participating!</h1>
                   </div>
        },
            component = paths[path];
        if (!component){
            throw new Error('Invalid path name ' + path);
        }
        return paths[path];
    },
    render: function(){
        return this.pathToComponent(this.state.view);
    }
});

var images = _.map(["apple.png",
                    "bananas.png",
                    "carrots.png",
                    "corn.png",
                    "grapes.png",
                    "lemon.png",
                    "lettuce.png",
                    "mushrooms.png",
                    "orange.png",
                    "peach.png",
                    "peanuts.png",
                    "pear.png",
                    "peas.png",
                    "pineapple.png",
                    "potato.png",
                    "pumpkin.png",
                    "strawberries.png",
                    "tomato.png"], function(basename){
                        return "static/img/fruits/" + basename;
                    }),
    all = [0,1,2,3,4,5,6,7,8],
    dec = function(x) { return x -  1; },
    practiceTrials = _.map([[1, 5],
                            [2, 8],
                            [3, 1, 6],
                            [4, 7, 3],
                            [4, 9],
                            [4, 2, 4],
                            [6, 3, 1],
                            [6, 5, 7, 6],
                            [9, 8, 4, 9],
                            [9, 2, 3]
                           ], function(data, n){
                               var seq = data.slice(1).map(dec),
                                   maskSize = data[0] - seq.length,
                                   maskPool = _.reject(all, function(i){
                                       return _.contains(seq, i);
                                   }),
                                   mask = _.shuffle(maskPool).slice(0, maskSize);
                               mask = mask.concat(seq);
                               // console.log(seq, mask);
                               return { mask: mask,
                                        sequence: seq,
                                        index: ['P', n + 1].join(''),
                                        images: _.shuffle(images).slice(0, mask.length)
                                      };
                           }),
    experimentalTrials = _.map([[7, 4],
                                [8, 6],
                                [3, 5, 1],
                                [6, 3],
                                [1, 9],
                                [5, 8, 4],
                                [9, 1, 2],
                                [2, 7, 6, 3],
                                [4, 5, 9],
                                [8, 4, 3],
                                [6, 8, 7, 2],
                                [4, 9, 8, 6],
                                [9, 6, 5, 7, 8],
                                [3, 4],
                                [1, 2, 9, 5],
                                [7, 3, 2, 1]], function(seq, n){
                                    return {
                                        mask: all,
                                        sequence: seq.map(dec),
                                        index: ['E', n + 1].join(''),
                                        images: _.shuffle(images).slice(0, all.length)
                                    };
                                }),
    trials = practiceTrials.concat(experimentalTrials),
    imageCache = new Array();

var fsm = StateMachine.create({
    initial: 'init',
    events: [
        { name: 'start',  from: 'init',                          to: 'dataCollect'},

        { name: 'submitData',  from: 'dataCollect',              to: 'expInput'},

        { name: 'cellClick',   from: 'expInput',                 to: 'expInput'},
        { name: 'cellClick',   from: 'partInput',                to: 'partInput'},

        { name: 'switchUser',  from: 'expInput',                 to: 'partInput'},
        { name: 'switchUser',  from: 'partInput',                to: 'expInput'},

        { name: 'finishTrial', from: ['expInput', 'partInput'],  to: 'feedback'},
        { name: 'nextTrial',   from: 'feedback',                 to: 'expInput'},
        { name: 'end',   from: 'feedback',                       to: 'end'}
    ],
    callbacks: {
        history: [],
        onbeforeevent: function(event){
            if (event === 'start'){
                // The component is passed in as an argument to start, we don't
                // want to try to serialize this.
                return;
            }
            var eventArgs = Array.prototype.slice.call(arguments, 0)
                    .filter(function(x){ return typeof x.nodeName === 'undefined'; });
            // console.log(eventArgs);
            // console.log(eventArgs.map(function(i) { return typeof i; }));
            var now = Date.now(),
                data = {state: this.comp ? this.comp.state : null,
                        event: eventArgs},
                dataString = JSON.stringify(data);
            // console.log('EVENT', data);
            utils.post('/event', [ "uid=", uid,
                                   "&timestamp=", now,
                                   "&data=", dataString].join(''));
            // this.history.push(entry);
        },
        onsubmitData: function(event, from, to, data){
            this.comp.setState({experimenter: data.expId,
                                participant: data.partId});
            // console.log('arguments', arguments);
            utils.fullscreen(document.body);
        },
        onbeforestart: function(event, from, to, comp){
            this.comp = comp;
            utils.post('/props', [ "uid=", uid,
                                   "&timestamp=", Date.now(),
                                   "&data=", JSON.stringify({props: decycle(comp.props)})].join(''));
            // console.log('EVENT', comp.props);
        },
        ondataCollect: function(event, from, to){
            this.comp.setState({view: 'dataCollect'});
        },
        oncellClick: function(event, from, to, cell, element){
            // console.log('clicked cell: ', cell.index, Date.now());
            if(cell.touchable){
                var cells = this.comp.state.cells.slice(),
                    that = this;
                utils.pop();
                if(cell.animating){
                    // re-trigger fading animation
                    element.classList.remove('fader');
                    window.setTimeout(function(){ element.classList.add('fader'); }, 1);
                }
                cell.touchable = false;
                cell.animating = true;
                cells[cell.index] = cell;
                var t = Date.now();
                this.comp.setState({cells: cells}, function(){
                    utils.addListeners(element, ['animationend', 'webkitAnimationEnd'], function(){
                        console.log('time delta', Date.now() - t);
                        var cells = that.comp.state.cells.slice();
                        cell.animating = false;
                        cells[cell.index] = cell;
                        that.comp.setState({cells: cells});
                    });
                });
            }
        },
        onbeforenextTrial: function(event, from, to){
            this.comp.setState({trialNumber: this.comp.state.trialNumber + 1});
        },
        inputHandler: function(event, from, to, user){
            var trial = this.comp.currentTrial(),
                cells = (from === "feedback" ? this.comp.makeCells(trial)
                         // avoid overwriting cells that might be in the middle
                         // of an animation
                         : this.comp.refreshedCells(this.comp.state.cells));
            this.comp.setState({activeUser: user,
                                view: 'grid',
                                cells: cells});
        },
        onexpInput: function(event, from, to){
            this.inputHandler(event, from, to, 'E');
        },
        onpartInput: function(event, from, to){
            this.inputHandler(event, from, to, 'P');
        },

        onfeedback: function(event, from, to){
            var that = this;
            that.comp.setState({view: 'feedback'}, function(){
                window.setTimeout(function(){
                    if(that.comp.finished()){
                        that.end();
                    } else {
                        that.nextTrial();
                    }
                }, 1000);
            });
            return StateMachine.ASYNC;
        },

        onend: function(event, from, to){
            this.comp.setState({view: 'end'});
            utils.post('/dump-data', ["uid=", uid].join(''));
        }
    }});
window.fsm = fsm;
window.ondragstart = function(){
    return false;
};
window.onload = function(){
    var preload = function(image) {
        var img = new Image();
        img.src = image;
	imageCache.push(img);
    };

    _.map(images, preload);
    // console.log(trials);
    React.renderComponent(<Experiment trials={trials}
                                      fsm={fsm}/>,
                          document.getElementById('app-container'));
};
