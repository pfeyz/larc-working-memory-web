/*global require, module */

module.exports = {
    fadein: fadein,
    pop: pop,
    addListeners: addListeners,
    post: post,
    fullscreen: fullscreen
};

function post(url, data){
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send(data);
}

function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}

function addListeners(obj, eventList, callback){
    eventList.forEach(function(event){
	obj.addEventListener(event, callback);
    });
}

// seq([f1, f2]) -> f(x){f1(x); f2(x)}
function seq(funs){
    return function(){
	var args = arguments;
	Array.prototype.slice.call(funs).forEach(function(fun){
	    fun.apply(null, args);
	});
    };
}

function fadein(el){
    assert(el.tagName === 'IMG');
    el.className = 'fader';
    addListeners(el, ['animationend', 'webkitAnimationEnd'], function(){
	el.className = '';
    });
}

function pop(){
    if(!pop.audioPlayers){
        pop.playerIndex = 0;
        pop.audioPlayers = document.getElementsByClassName('sound-effect');
        Array.prototype.forEach.call(pop.audioPlayers, function(p){
            p.volume = 0.75;
        });
    }
    var el = pop.audioPlayers[pop.playerIndex];
    pop.playerIndex = (pop.playerIndex + 1) % pop.audioPlayers.length;
    el.addEventListener('ended', function(){
	el.load();
    });
    if (el.currentTime > 0){
	el.load();
    }
    el.play();
}

function fullscreen(elem){
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }
}
