from app import dump_data, get_datalist

if __name__ == "__main__":
    for uid in get_datalist():
        dump_data(uid)
